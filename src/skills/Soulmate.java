package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Soulbound;
import status.Stsflag;

public class Soulmate extends Skill {

    public Soulmate(Character self){
        super("Soulmate",self);
    }

    @Override
    public boolean requirements(Character user) {
        return self.getPure(Attribute.Contender)>=7;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return c.stance.kiss(self)&&self.canAct()&&self.canSpend(50)&&!self.is(Stsflag.soulbound);
    }

    @Override
    public String describe() {
        return "Share your sense of touch with your opponent";
    }

    @Override
    public void resolve(Combat c, Character target) {
        if(self.human()){
            c.write(self,deal(0,Result.normal,target));
        }else if(target.human()){
            c.write(self,receive(0,Result.normal,target));
        }
        int duration = self.getPure(Attribute.Contender)/2;
        self.add(new Soulbound(self,target,duration),c);
    }

    @Override
    public Skill copy(Character user) {
        return new Soulmate(user);
    }

    @Override
    public Tactics type() {
        return Tactics.status;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "You pull "+target.name()+" close and capture her lips. While she's focused on the kiss, you establish a single directional mana link. It takes her a moment to " +
                "notice the new sensation and even longer to understand it. Until this wears off, she'll feel anything she does to your body.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return self.name()+" kisses you softly and romantically, slowly drawing you into her embrace. As she pulls away, you feel something unnatural, but you can't put your finger on it. " +
                "She grins mischievously and softly runs her hand down her body. You feel a jolt of pleasure as if she had touched you.";
    }
}
