package skills;

import status.Rewired;
import status.Shamed;
import global.Global;
import items.Component;
import items.Item;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class ShortCircuit extends Skill {

	public ShortCircuit(Character self) {
		super("Short-Circuit", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science)>=15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self)&&target.nude()&&self.has(Component.Battery, 3);
	}

	@Override
	public String describe() {
		return "A blast of energy with confused your opponent's nerves so she can't tell pleasure from pain: 3 Batteries.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Component.Battery, 3);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.add(new Rewired(target,4+Global.random(3)),c);
	}

	@Override
	public Skill copy(Character user) {
		return new ShortCircuit(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You send a light electrical current through "+target.name()+"'s body, disrupting her nerve endings. She'll temporarily feel pleasure as pain and pain as pleasure.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" aims a devices at you and you feel a strange shiver run across your skin. You feel indescribably weird. She's done something to your sense of touch.";
	}

}
