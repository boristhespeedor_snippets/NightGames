package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import items.Component;
import status.Bound;

public class MatterConverter extends Skill {

	public MatterConverter(Character self) {
		super("Matter Converter", self);

	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science)>=30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.reachTop(self)&&!c.stance.reachTop(target)&&c.stance.dom(self)&&!self.nude()&&self.has(Component.Battery,10);
	}

	@Override
	public String describe() {
		return "Converts all your remaining clothing to a powerful binding material: 10 Battery";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Component.Battery, 10);
		int m = self.top.size() + self.bottom.size();
		self.nudify();
		
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.add(new Bound(target,20 + m*10,"Super-web"),c);
	}

	@Override
	public Skill copy(Character user) {
		return new MatterConverter(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You vacuum up your own clothing into your multitool, where it is converted into Super-web material. "
				+ "Grabbing %s's wrists, you cover them in the webbing, binding them in place.",
				target.name());
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s's clothes quickly vanish off %s body as they're sucked into %s wrist device. "
				+ "Said device immediately spits a clump of sticky string that binds your wrists tightly in place. "
				+ "You pull against the tangled binding, but you can't get loose.", 
				self.name(),self.possessive(false),self.possessive(false));
	}

}
