package skills;

import global.Global;
import stance.Facesitting;
import stance.Stance;
import status.Enthralled;
import status.Shamed;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class MountFace extends Skill {

	public MountFace(Character self) {
		super("Mount Face", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getLevel()>=15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&&c.stance.dom(self)
				&&c.stance.reachTop(self)
				&&self.pantsless()
				&&!c.stance.penetration(self)&&!c.stance.penetration(target)
				&&c.stance.prone(target)
				&&(c.stance.en!=Stance.facesitting)
				&&!self.has(Trait.shy);
	}

	@Override
	public String describe() {
		return "Mount your opponent's face to demonstrate your superiority while you pleasure them.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.has(Trait.succubus)){
			if (Global.random(5) == 0) {
				target.tempt(5);
				if(self.get(Attribute.Dark)>=6&&Global.random(2)==0){
					if(self.human()){
						c.write(self,deal(0,Result.special,target));
					}else if(target.human()){
						c.write(self,receive(0,Result.special,target));
					}
					target.add(new Enthralled(target,self),c);
				}
				else{
					if(self.human()){
						c.write(self,deal(0,Result.strong,target));
					}else if(target.human()){
						c.write(self,receive(0,Result.strong,target));
					}
				}
			}
			else{
				if(self.human()){
					c.write(self,deal(0,Result.normal,target));
				}else if(target.human()){
					c.write(self,receive(0,Result.normal,target));
				}
			}
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
		}
		if (Global.random(3) == 0) {
			target.add(new Shamed(target),c);
		}
		self.buildMojo(10);
		self.pleasure(Global.random(self.get(Attribute.Perception)/2)+target.get(Attribute.Seduction)/5,Anatomy.genitals,c);
		c.stance=new Facesitting(self,target);
	}

	@Override
	public Skill copy(Character user) {
		return new MountFace(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking; // tactics-wise this is more sensible than pleasure
	}

	public String toString(){
		if(self.hasBalls()){
			return "Mount Face";
		}
		else{
			return "Facesitting";
		}
	}
	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(self.hasBalls()){
			if(modifier==Result.special){
				return "You kneel over "+target.name()+"'s face and dunk your balls into her mouth. She can do little except lick them submissively, which does feel " +
						"pretty good. She's so affected by your manliness that her eyes glaze over and she falls under your control.";
			}
			else if(modifier==Result.strong){
				return "You crouch over "+target.name()+"'s face and dunk your balls into her mouth. She can do little except lick them submissively, which does feel " +
						"pretty good. Your powerful musk is clearly starting to turn her on.";			}
			else{
				return "You crouch over "+target.name()+"'s face and dunk your balls into her mouth. She can do little except lick them submissively, which does feel " +
						"pretty good.";
			}
		}
		else{
			if(modifier==Result.special){
				return "You straddle "+target.name()+"'s face and grind your pussy against her mouth, forcing her to eat you out. Your juices take control of her lust and " +
						"turn her into a pussy licking slave. Ooh, that feels good. You better be careful not to get carried away with this.";
			}
			else if(modifier==Result.strong){
				return "You straddle "+target.name()+"'s face and grind your pussy against her mouth, forcing her to eat you out. She flushes and seeks more of your tainted juices. " +
						"Ooh, that feels good. You better be careful not to get carried away with this.";
			}
			else{
				return "You straddle "+target.name()+"'s face and grind your pussy against her mouth, forcing her to eat you out. Ooh, that feels good. You better be careful " +
						"not to get carried away with this.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(self.hasBalls()){
			if(modifier==Result.special){
				return self.name()+" straddles your head and dominates you by putting her balls in your mouth. For some reason, your mind seems to cloud over and you're " +
						"desperate to please her. She gives a superior smile as you obediently suck on her nuts.";
			}
			else if(modifier==Result.strong){
				return self.name()+" straddles your head and dominates you by putting her balls in your mouth. Despite the humiliation, her scent is turning you on incredibly. " +
						"She gives a superior smile as you obediently suck on her nuts.";
			}
			else{
				return self.name()+" straddles your head and dominates you by putting her balls in your mouth. She gives a superior smile as you obediently suck on her nuts.";
			}
		}
		else{
			if(modifier==Result.special){
				return self.name()+" straddles your face and presses her pussy against your mouth. You open your mouth and start to lick her freely offered muff, but she just smiles " +
						"while continuing to queen you. As you swallow her juices, you feel her eyes start to bore into your mind. You can't resist her. You don't even want to.";
			}
			else if(modifier==Result.strong){
				return self.name()+" straddles your face and presses her pussy against your mouth. You open your mouth and start to lick her freely offered muff, but she just smiles " +
						"while continuing to queen you. You feel your body start to heat up as her juices flow into your mouth. She's giving you a mouthful of aphrodisiac straight from " +
						"the source.";
			}
			else{
				return self.name()+" straddles your face and presses her pussy against your mouth. You open your mouth and start to lick her freely offered muff, but she just smiles " +
						"while continuing to queen you. She clearly doesn't mind accepting some pleasure to demonstrate her superiority.";
			}
		}
	}

}
