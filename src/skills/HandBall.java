package skills;

import characters.*;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Stance;
import status.Masochistic;

public class HandBall extends Skill{
    public HandBall(Character self) {
        super("Hand Ball", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 9;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return c.stance.reachBottom(self)&&(target.pantsless()||(self.has(Trait.dexterous)&&target.bottom.size()<=1))&&target.hasBalls()&&self.canAct()&&c.stance.behind(self);
    }

    @Override
    public String describe() {
        return "Grab your opponent's bits and teach them the pleasures of masochism.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        int m = Global.random(self.get(Attribute.Footballer))+target.get(Attribute.Perception)-(2*target.bottom.size());
        Result type = Result.normal;
        if(self.human()){
            if(target.hasBalls()){
                type = Result.strong;
            }
            c.write(self,deal(m,type,target));
        }
        else if(target.human()){
            c.write(self,receive(m,type,target));
        }
        target.tempt(Global.random(4)+self.get(Attribute.Seduction)/4,c);
        target.pain(m, Anatomy.genitals,c);
        if(self.has(Trait.wrassler)){
            target.calm(m/4,c);
        }
        else{
            target.calm(m/2,c);
        }
        self.buildMojo(10);
        target.emote(Emotion.angry,15);
        self.add(new Masochistic(self),c);
        if(c.effectRoll(this,self,target,self.get(Attribute.Footballer))){
            target.add(new Masochistic(target),c);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new HandBall(user);
    }

    @Override
    public Tactics type() {
        return Tactics.damage;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier==Result.strong){
            return "You grab "+target.name()+"'s balls roughly, taking the fight out of her. While she's being cooperative, you pull her into a passionate kiss. " +
                    "After swapping spit for a while, "+(Global.random(2)==0?"you give her testicles a hard squeeze for good measure.":"You give her sack a couple light slaps.");
        }
        return "You grab "+target.name()+"'s vulva and squeeze hard enough that she knows you aren't about to pleasure her. She doesn't resist when you pull her into a passionate kiss. " +
                "After swapping spit for a while, you isolate her sensitive clitoris and "+(Global.random(2)==0?"give it a painful pinch.":"give it a few light smacks.");
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return self.name()+" grabs your ballsack firmly, and you freeze instinctively. <i>\"You know what I could do to these tender things? It would hurt so good!\"</i> " +
                "Before you can reply, she kisses you deeply. " +
                (Global.random(2)==0?"You groan in pain as she tightens her grip on your testicles.":"Without breaking the kiss, she slaps your tender balls three times.");
    }
}
