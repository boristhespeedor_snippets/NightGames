package skills;

import global.Global;
import status.Stsflag;
import combat.Combat;
import combat.Result;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

public class Frottage extends Skill{

	public Frottage(Character self) {
		super("Frottage", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=26;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.sub(self)&&!c.stance.penetration(self)&&target.pantsless()&&((self.hasDick()&&self.pantsless())||self.has(Trait.strapped));
	}

	@Override
	public String describe() {
		return "Rub yourself against your opponent";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int x = Global.random(3+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception));
		if(self.human()){
			if(target.hasDick()){
				c.write(self,deal(x,Result.special,target));
			}
			else{
				c.write(self,deal(x,Result.normal,target));
			}
			x = self.bonusProficiency(Anatomy.genitals, x);
			self.pleasure(x/2,Anatomy.genitals,c);			
		}
		else if(self.has(Trait.strapped)){
			if(target.human()){
				c.write(self,receive(x,Result.special,target));
			}
			x = self.bonusProficiency(Anatomy.toy, x);
			target.buildMojo(-10);
		}
		else{
			if(target.human()){
				c.write(self,receive(x,Result.normal,target));
			}
			x = self.bonusProficiency(Anatomy.genitals, x);
			self.pleasure(x/2,Anatomy.genitals,c);
		}
		target.pleasure(x,Anatomy.genitals,c);
		self.buildMojo(20);
		self.emote(Emotion.horny, 15);
		target.emote(Emotion.horny, 15);
	}

	@Override
	public Skill copy(Character user) {
		return new Frottage(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return "You tease "+target.name()+"'s penis with your own, dueling her like a pair of fencers.";
		}
		else{
			return "You press your hips against "+target.name()+"'s, rubbing her nether lips and clit with your dick.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return self.name()+" thrusts her hips to prod your delicate jewels with her strap-on dildo. As you flinch and pull your hips back, she presses the toy against your cock, teasing your sensitive parts.";
		}
		else{
			return self.name()+" pushes her girl-cock against your the sensitive head of your member, dominating your manhood by pleasuring it with her own.";
		}
	}

}
