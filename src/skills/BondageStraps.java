package skills;

import status.Stsflag;
import items.Clothing;
import items.Component;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class BondageStraps extends Skill {

	public BondageStraps(Character self) {
		super("Bondage Straps", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&self.is(Stsflag.bondage)&&self.nude()&&self.canSpend(20)&&!c.stance.penetration(self)&&!c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Summon a kinky outfit suitable for your bondage fetish: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.canWear(Clothing.bharness)){
			self.wear(Clothing.bharness);
		}
		if(self.canWear(Clothing.bstraps)){
			self.wear(Clothing.bstraps);
		}
		self.spendMojo(20);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new BondageStraps(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "The power of your fetish manifests as a kinky leather body harness, digging tightly into your body.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" glows with kinky energy and is suddenly covered with tight, leather straps.";
	}

}
