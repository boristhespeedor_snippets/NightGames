package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Primed;

public class Rewind extends Skill {

	public Rewind(Character self) {
		super("Rewind", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal)>=10;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&!c.stance.prone(self)&&self.canAct()&&self.getStatusMagnitude("Primed")>=8;
	}

	@Override
	public String describe() {
		return "Rewind your personal time to undo all damage you've taken: 8 charges";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.add(new Primed(self,-8));
		self.getArousal().empty();
		self.getStamina().fill();
		self.clearStatus();
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.emote(Emotion.confident, 25);
		self.emote(Emotion.dominant, 20);
		target.emote(Emotion.nervous, 10);
		target.emote(Emotion.desperate, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new Rewind(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("It takes a lot of time energy, but you manage to rewind your physical condition back to the very start "
				+ "of the match, removing all damage you've taken.");
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s hits a button on %s wristband and suddenly seems to completely recover. It's like nothing "
				+ "you've done even happened.",self.name(),self.possessive(false));
	}

}
