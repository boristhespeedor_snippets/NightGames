package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import status.Stsflag;

public class OpenGoal extends Skill{
    public OpenGoal(Character self) {
        super("Open Goal", self);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer)>=30;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return c.stance.feet(self)&&self.canAct()&&!self.has(Trait.sportsmanship)
                &&(!c.stance.prone(self)||self.has(Trait.dirtyfighter))
                &&!c.stance.penetration(self)
                &&target.is(Stsflag.masochism)&&target.is(Stsflag.horny);

    }

    @Override
    public String describe() {
        return "Convince a horny, masochistic opponent to let you finish them off with a hard kick to the groin.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        if(self.human()){
            c.write(self,deal(0,Result.normal,target));
        }
        else if(target.human()){
            c.write(self,receive(0,Result.normal,target));
        }
        target.pain(self.get(Attribute.Footballer)*10, Anatomy.genitals,c);
    }

    @Override
    public Skill copy(Character user) {
        return new OpenGoal(user);
    }

    @Override
    public Tactics type() {
        return Tactics.damage;
    }

    public int speed(){
        return 2;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "You lean in to "+target.name()+"'s ear and suggest that you'll get too turned on to fight back if you can just kick her in the groin. " +
                "In her lust-addled state, she can't resist temptation and spreads her legs for you. You take a step back to get " +
                "the right distance, before giving her your best kick to the crotch. Her eyes roll back in her head as she " +
                "lets out a quiet noise between a moan and a whimper.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return self.name()+" gives you a naughty smile. <i>\"Come on, baby. Let me give your big balls my best kick. " +
                "It'll get me so hot, I'll probably lose on the spot.\"</i> Your overwhelming lust and masochism " +
                "agree with her suggestion, while the dissenting voice of reason in your head is strangely quiet. " +
                self.name()+" winds back and gives you a devastating punt in the groin. You howl in agony, doubling over " +
                "and cupping your throbbing testicles.";
    }
}
