package skills;

import items.Component;
import items.Item;
import pet.Slime;
import characters.Attribute;
import characters.Character;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class SpawnSlime extends Skill {

	public SpawnSlime(Character self) {
		super("Create Slime", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science)>=3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self)&&self.pet==null&&self.has(Component.Battery);
	}

	@Override
	public String describe() {
		return "Creates a mindless, but living slime to attack your opponent: 1 Battery";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Component.Battery, 1);
		int power = 3+self.bonusPetPower()+(self.get(Attribute.Science)/10);
		int ac = 3+self.bonusPetEvasion()+(self.get(Attribute.Science)/10);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.pet=new Slime(self,power,ac);
		c.offerImage("Slime.png", "Art by AimlessArt");
	}

	@Override
	public Skill copy(Character user) {
		return new SpawnSlime(user);
	}

	@Override
	public Tactics type() {
		return Tactics.summoning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You dispense blue slime on the floor and send a charge through it to animate it. The slime itself is not technically alive, but an extension of a larger " +
				"creature kept in Jett's lab.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" points a device at the floor and releases a blob of blue slime. The blob starts to move like a living thing and briefly takes on a vaguely humanoid shape " +
				"and smiles at you.";
	}

}
