package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;

public class PleasureBomb extends Skill {

	public PleasureBomb(Character self) {
		super("Pleasure Bomb", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ki)>=30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pantsless()&&c.stance.reachBottom(self)&&c.stance.dom(self)&&self.canAct()&&!c.stance.penetration(self);
	}

	@Override
	public String describe() {
		return "Consumes all your stamina to deliver a slow, but powerful pleasure attack. More effective against stunned opponents.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(c.attackRoll(this, self, target)){
			int m = 20 + self.get(Attribute.Seduction) + self.get(Attribute.Ki) + Global.random(target.get(Attribute.Perception));
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			self.getStamina().empty();
			if(!target.canAct()){
				m *= 2;
			}
			m = self.bonusProficiency(Anatomy.fingers, m);
			target.pleasure(m, Anatomy.genitals,c);
			target.emote(Emotion.horny, 20);
		}else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
			target.emote(Emotion.nervous, 10);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new PleasureBomb(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	public int speed(){
		return 1;
	}
	public int accuracy(){
		return 1;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return String.format("You charge up your hand with all of your Ki and aim for %s's groin. "
					+ "At the last second %s jerks %s hips out of the way and the energy disperses harmlessly, leaving you drained.",
					target.name(),target.pronounSubject(false),target.possessive(false) );
		}else if(target.hasDick()){
			return String.format("You charge up your hand with all of your Ki and aim for %s's groin. You grab %s penis and pour your energy "
					+ "into it while stroking rapidly. %s can't help moaning as you overwhelm %s with intense pleasure.",
					target.name(),target.possessive(false),target.pronounSubject(true),target.pronounTarget(false) );
		}else{
			return String.format("You charge up your hand with all of your Ki and aim for %s's groin. You focus the massive amount of energy "
					+ "into your fingertips as you rub her clit directly. She can't help moaning as you overwhelm %s with intense pleasure.",
					target.name(),target.pronounTarget(false) );
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return String.format("%s raises %s hand and it begins to glow with tremendous power. %s grabs for your dick, but you manage to "
					+ "dodge out of the way at the last moment.",
					self.name(),self.possessive(false),self.pronounSubject(true));
		}else{
			return String.format("%s raises %s hand and it begins to glow with tremendous power. %s grabs your cock, pumping energetically. "
					+ "Your hips buck involuntarily as you're suddenly assaulted with intense pleasure. The sensation is so overwhelming, it's "
					+ "almost painful.",
					self.name(),self.possessive(false),self.pronounSubject(true));
		}
	}

}
