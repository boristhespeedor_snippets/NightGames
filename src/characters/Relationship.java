package characters;

import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

public class Relationship {
    private ID person1;
    private ID person2;
    private int value;

    public Relationship(ID p1, ID p2){
        person1 = p1;
        person2 = p2;
        value = 0;
    }
    public Relationship(ID p1, ID p2, int value){
        person1 = p1;
        person2 = p2;
        this.value = value;
    }
    public int get(){return value;}

    public void set(int value){
        this.value = value;
    }

    public void add(int value){
        this.value += value;
    }

    public boolean contains(ID person){
        return person1==person || person2==person;
    }

    public boolean is(ID p1, ID p2){
        return contains(p1) && contains(p2);
    }

    public ID getPartner(ID character){
        if(person1==character){
            return person2;
        }
        if(person2==character){
            return person1;
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() == Relationship.class){
            Relationship other = (Relationship)obj;
            return other.contains(person1) && other.contains(person2);
        }
        return false;
    }
}
