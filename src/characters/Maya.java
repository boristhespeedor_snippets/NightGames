package characters;

import daytime.Daytime;
import global.*;
import items.Clothing;
import items.Toy;
import items.Trophy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import Comments.CommentGroup;
import Comments.CommentSituation;
import skills.*;
import status.Drowsy;
import status.Energized;
import actions.Action;
import actions.Movement;

import combat.Combat;
import combat.Result;
import status.Stsflag;

public class Maya implements Personality {
	public NPC character;
	public Maya() {
		this.character = new NPC("Maya",ID.MAYA, 50, this);
		this.character.outfit[0].add(Clothing.camisole);
		this.character.outfit[0].add(Clothing.blouse);
		this.character.outfit[1].add(Clothing.lacepanties);
		this.character.outfit[1].add(Clothing.skirt);
		character.closet.add(Clothing.blouse);
		character.closet.add(Clothing.skirt);
		character.closet.add(Clothing.camisole);
		character.closet.add(Clothing.lacepanties);
		this.character.change(Modifier.normal);
		this.character.set(Attribute.Dark, 40);
		this.character.set(Attribute.Seduction, 66);
		this.character.set(Attribute.Cunning, 39);
		this.character.set(Attribute.Speed, 17);
		this.character.set(Attribute.Power, 36);
		this.character.set(Attribute.Hypnosis, 8);
		this.character.setUnderwear(Trophy.MayaTrophy);
		this.character.getStamina().gain(160);
		this.character.getArousal().gain(300);
		this.character.getMojo().gain(300);
		character.add(Trait.female);
		character.add(Trait.darkpromises);
		character.add(Trait.greatkiss);
		character.add(Trait.Confident);
		character.add(Trait.event);
		character.add(Trait.cursed);
		character.gain(Toy.Onahole2);
		character.gain(Toy.Dildo2);
		character.gain(Toy.Tickler2);
		character.gain(Toy.Crop2);
		Global.gainSkills(this.character);
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 4);
		character.strategy.put(Emotion.sneaking, 2);
		character.arousal.empty();
		character.preferredSkills.add(Suggestion.class);
		character.preferredSkills.add(Footjob.class);
		character.preferredSkills.add(Whisper.class);
		character.preferredSkills.add(Stomp.class);
		character.preferredSkills.add(Spank.class);
		if(Global.checkFlag(Flag.PlayerButtslut)){
			character.preferredSkills.add(AssFuck.class);
			character.preferredSkills.add(Strapon.class);
			character.preferredSkills.add(FingerAss.class);
			character.gain(Toy.Strapon2);
		}

	}
	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
		for(Skill a:available){
			if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Ass Fuck")){
				mandatory.add(a);
			}
			if(character.is(Stsflag.orderedstrip)){
				if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
					mandatory.add(a);
				}
			}
			if(character.has(Trait.strapped)){
				if((a.toString()=="Mount")){
					mandatory.add(a);
				}
				if(a.toString()=="Turn Over"){
					mandatory.add(a);
				}
			}
		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public NPC getCharacter() {
		return this.character;
	}

	@Override
	public void rest(int time, Daytime day) {
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		available.add("Play Video Games");
		for(int i=0;i<time-3;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		character.visit(3);
	}

	@Override
	public String bbLiner() {
		return "Maya looks at you sympathetically. <i>\"Was that painful? Don't worry, you aren't seriously injured. Our Benefactor protects us.\"</i>";
	}

	@Override
	public String nakedLiner() {
		return "Maya smiles, unashamed of her nudity. <i>\"Well done. Not many participants are able to get my clothes off anymore. You'll at least be able to look at a " +
				"naked woman while you orgasm.\"</i>";
	}

	@Override
	public String stunLiner() {
		return "You think you see something dangerous flicker in Maya's eyes. <i>\"Well done. I may need to get a little serious.\"</i>";
	}

	@Override
	public String taunt() {
		return "Maya gives you a look of gentle disapproval. <i>\"You aren't putting up much of a fight, are you? Aren't you a little overeager to cum?\"</i>";
	}

	@Override
	public String victory(Combat c, Result flag) {
		Character target;
		if(c.p1==character){
			target=c.p2;
		}
		else{
			target=c.p1;
		}
		target.add(new Drowsy(target,5));
		character.arousal.empty();
		character.add(new Energized(character,10));
		if(flag==Result.anal){
			if(Global.getValue(Flag.PlayerAssLosses)==0){
				Global.flag(Flag.tookAssMaya);
				Global.modCounter(Flag.PlayerAssLosses, 1);
				return "Maya completely outmatches you. It felt like one moment she was toying with your cock, the next moment she was behind you and whispering promises "
						+ "into your ear, and the next she was pinning you down and sensually pegging you. You barely even had a chance to fight back, and in an "
						+ "embarrassingly short time your cock surrenders a sticky white flag in her hands as you cum disgracefully to her anal attack. "
						+ "You slump down to the floor as you catch your breath. Maya smiles gently at you, but there's a predatory glint in her eyes. <i>\"Don't feel bad. " +
						"You put up an impressive fight, given your inexperience - I'm amazed you kept your back door intact all this time, but somebody had to break you in eventually.\"</i> She seems to be looking into your soul. You feel an almost animal instinct to look away, " +
						"but can't bring yourself to break eye contact. <i>\"I hope you learned something, because the lesson is over now. It's time for me to take the other thing I came for.\"</i><p>" +
						"You wake up on the floor, naked and alone. It doesn't seem like much time passed, but you feel lethargic and almost hung-over. "
						+ "Your mind wander back to the harsh yet satisfying treatment Maya gave your asshole. On the one hand, you feel like you've been missing out "
						+ "on a world of pleasure all the time. On the other hand, you feel proud you made it this far and were able to give your first time to an "
						+ "overpoweringly dominant and skilled lady like Maya.";
			}
			Global.modCounter(Flag.PlayerAssLosses, 1);
			return "Maya completely outmatches you. It felt like one moment she was toying with your cock, the next moment she was behind you and whispering promises "
					+ "into your ear, and the next she was pinning you down and sensually pegging you. You barely even had a chance to fight back, and in an "
					+ "embarrassingly short time your cock surrenders a sticky white flag in her hands as you cum disgracefully to her anal attack. "
					+ "You slump down to the floor as you catch your breath. Maya smiles gently at you, but there's a predatory glint in her eyes. <i>\"Don't feel bad. " +
					"You put up an impressive fight, given your inexperience - I don't use my strapon on just any boy.\"</i> She seems to be looking into your soul. You feel an almost animal instinct to look away, " +
					"but can't bring yourself to break eye contact. <i>\"I hope you learned something, because the lesson is over now. It's time for me to take the other thing I came for.\"</i><p>" +
					"You wake up on the floor, naked and alone. It doesn't seem like much time passed, but you feel lethargic and almost hung-over. What the hell " +
					"happened to you?";
		}
		return "Maya completely outmatches you. How were you supposed to deal with someone this skilled? Your cock spurts a sticky white flag in her hands as you " +
				"cum. You slump down to the floor as you catch your breath. Maya smiles gently at you, but there's a predatory glint in her eyes. <i>\"Don't feel bad. " +
				"You put up an impressive fight, given your inexperience.\"</i> She seems to be looking into your soul. You feel an almost animal instinct to look away, " +
				"but can't bring yourself to break eye contact. <i>\"I hope you learned something, because the lesson is over now. It's time for me to take what I came for.\"</i><p>" +
				"You wake up on the floor, naked and alone. It doesn't seem like much time passed, but you feel lethargic and almost hung-over. What the hell " +
				"happened to you?";
	}
	@Override
	public String defeat(Combat c, Result flag) {
		Character opponent=c.getOther(character);
		declareGrudge(character,c);
		Global.flag(Flag.Mayabeaten);
		if(flag == Result.anal){
			return "Maya is not an opponent to be taken lightly. Even with your cock impaled in her ass and her wrists gripped firmly "
					+ "in your hands, she proves herself to be a dangerous opponent by managing the strength and flexibility to lift "
					+ "her foot up. Her toes manage to find your balls, and she wastes no time before expertly massaging them, "
					+ "bringing you closer and closer to orgasm. She even somehow manages to find the presence of mind to rhythmically "
					+ "contract her rectal muscles, squeezing your cock in time with each of your thrusts.<p>"
					+ "Sensing that the battle is moments away from being lost, you decide you have no choice but to take a gamble on "
					+ "the chance that you can push her over the edge first. You release your grip on Maya's wrists and wrap your "
					+ "arms around her before she can react and make use of her sudden freedom. Your left hand reaches across her "
					+ "breasts, pinching her nipple between your fingers, while your right hand reaches down toward her pussy, "
					+ "seeking out her clit.<p>"
					+ "Maya lets out a sudden moan as your fingers make contact with her bud. Not wasting this chance, you rub it "
					+ "furiously while trying to focus your mind to resist the urge to climax too soon. You begin to fear it might "
					+ "be a losing battle, and are just moments away from losing control when a sharp cry pierces through the fog of "
					+ "your mind. Maya screams out in ecstasy as your fingers manage to tear an orgasm from her body. Just moments "
					+ "later, you let yourself go, emptying your balls into Maya's ass.<p>"
					+ "Panting, the two of you collapse onto the ground. Before long, Maya's pants turn into a soft chuckle. "
					+ "<i>\"I can't believe you did that,\"</i> she says, shaking her head. <i>\"For a freshman to defeat me is one "
					+ "thing, but to do it through anal? You're truly something else.\"</i><p>"
					+ "Still a bit short on breath, you pull yourself back, smiling at her as it fully dawns on you that you did "
					+ "indeed, somehow manage to win. That was a battle for the ages, though, and things could have easily gone the "
					+ "other way if you'd been a bit less careful or lucky.<p>"
					+ "<i>\"I'm curious though,\"</i> Maya says, rolling over onto her back and then sitting up to face you. She "
					+ "gives you a smile that might be just a bit playful as she asks, <i>\"Why do you choose to do it that way? "
					+ "Anal, I mean. It puts you at quite a disadvantage you know. Did you just want to be able to say you fucked me "
					+ "in the ass, even if you ended up losing? Or are you so much of an anal freak that you didn't think that far "
					+ "ahead? Or was it actually part of your strategy... throw me off-guard and then use that to your advantage?\"</i><p>"
					+ "You open your mouth for a moment as you debate answering, but Maya reaches her hand up and places a finger on "
					+ "your lips.<p>"
					+ "<i>\"Never mind,\"</i> she says. <i>\"I think I learned all I needed to just by looking into your eyes. You really do need to work on your poker face, but there's still plenty of time for that. But you still did win this match, so I'll make sure you're properly rewarded at the end of the night. Don't let it get to your head though - I won't let you win this easily again.\"</i>";
		}
		return "As you pleasure and caress Maya's body, she lets out a lovely gasp. You've finally got her on the ropes, and she knows it. Despite her experience, her body is still " +
				"subject to her sexual needs. She grasps for your dick in desperation, but you manage to ward off her silk-gloved hand. " +
				"Her body is rocked by a shudder of ecstasy and she lets out an orgasmic cry.<p>" +
				"You did it! You actually managed to beat Maya! She sits on the floor, staring at you in shock as she recovers from her climax. <i>\"Oh my. A freshman actually " +
				"made me orgasm. Either I was very careless, or you're no ordinary freshman.\"</i> She smiles approvingly. <i>\"Probably some of both. I'll see that Lilly " +
				"gives you a nice bonus.\"</i> She gently pulls you to the floor next to her. <i>\"I think you also deserve a more immediate reward.\"</i><p>" +
				"She climbs on top of you and presses her soft boobs against your cock. The head of your penis pokes out of her cleavage and she covers it with her mouth. " +
				"Oh God. This feels amazing. Her breasts thoroughly massage your shaft and her skilled tongue teases your sensitive glans. You writhe in exquisite pleasure. " +
				"You won't last long against her technique, but you want to enjoy it as long as possible. She sucks hard on your dick and you can't resist cumming in her " +
				"mouth.<p>" +
				"She swallows your semen as gracefully as she can and smiles at you. <i>\"I'm pleased to see you do enjoy my efforts. I was worried I might be losing my touch.\"</i>";
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "<i>\"Oh my. I don't think the boy had much of a chance to start with, but now the other girls are picking on him too.\"</i> She looks over your " +
					"helpless body and sides of her mouth curl slightly with amusement. She delicately extends her stocking-covered foot and teases the tip of your penis.<br>" +
					"<i>\"I think you'll enjoy this bullying, at least.\"</i> She rubs and squeezes your sensitive glans with her toes. Your hips buck involuntarily as she " +
					"keeps moving her foot to stimulate different spots on your shaft. How can she give you this kind of pleasure with just one foot? You knew she was experienced, " +
					"but this is insane! She steps down on your dick, grinding it with the sole of her foot. You can't hold on any more. Your cock throbs and you shoot your seed " +
					"onto her foot.";
		}
		else{
			if(assist.eligible(this.character)){
				assist.defeated(this.character);
			}
			return "Maya looks a bit surprised as you restrain her opponent. <i>\"You're going to side with me? I appreciate the thought.\"</i> She kneels down in front of "+target.name() +
					" and kisses her softly while staring deeply into her eyes. She turns her attention toward you and leans close to whisper in your ear. <i>\"But I don't need any help\"</i> " +
					"You don't notice her hand snake between your legs until she suddenly gives your balls a painful flick. The shock and surprise makes you lose your grip on "+target.name() +
					", who twists out of your arms and pins you to the floor. Suddenly you're the one being double-teamed. Her eyes have a certain glazed over appearance that tells you " +
					"she's under Maya's influence.<p>" +
					"Maya casually moves between your legs, where she can easily reach your dick and "+target.name()+"'s slit. You groan despite yourself as she starts to stroke your shaft " +
					"with both hands. Her mouth goes to work on the other girl's pussy. The two of you writhe in pleasure, completely at the mercy of the veteran sexfighter. "+target.name()+ 
					" kisses you passionately as you both cum simultaneously.";
		}
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "In the middle of your fight with "+assist.name()+", you feel an overwhelming presence approach from behind. She apparently senses it too, because you both stop and turn " +
					"to see Maya confidently walking towards you. She smiles and looks at the two of you. <i>\"Eenie... Meenie... Minie....\"</i> Her finger flicks in your direction. <i>\"Mo.\"</i><br>" +
					"You take a step back and prepare to defend yourself, but she's too fast. She reaches you in an instant and sweeps your feet out from under you, depositing you on the floor. She " +
					"pins your arms helplessly under her legs and sits casually on your chest. She's lighter than she looks, but you're still completely unable to move her. <i>\"Sorry, it's just your " +
					"bad luck. I could have helped you instead, but I don't do this very often anymore and I wanted to watch a cute boy orgasm up close. You should try to enjoy your defeat, it's just another " +
					"part of the Game.\"</i><br>" ;
		}
		else{
			return "In the middle of your fight with "+target.name()+", you feel an overwhelming presence approach from behind. She apparently senses it too, because you both stop and turn " +
					"to see Maya confidently walking towards you. She smiles and looks at the two of you. <i>\"Eenie... Meenie... Minie....\"</i> She points toward "+target.name()+". <i>\"Mo.\"</i><br>" +
					"Before the girl can react, Maya is on her and kisses her passionately. "+target.name()+" struggles for just a moment before going limp. Maya moves to licking and sucking " +
					"her neck, while she eases the helpless girl to the floor. She glances back at you and you feel a chill run down your spine as her eyes seem to bore into you. <i>\"Are you " +
					"going to make me do all the work? There's a lovely, naked girl in front of you who needs a good licking. Don't keep her waiting.</i><br>";
		}
	}
	
	public String watched(Combat c, Character target, Character viewer){
		return "Every other competitor in the Games, at least that you've seen, is hesitant at first. Nobody's ever sure what the other person has in store, especially lately. They're concerned, and right to be so. The stakes might just be embarrassment and a little less money at the end of the night, but there are still stakes.<p>"
				+ "Maya doesn't seem to care. She walks straight at "+target.name()+" with even strides and an expression of pleasant interest.<p"
				+ "<i>\"So,\"</i> Maya says, <i>\"what have you learned so far?\"</i><p>"
				+ ""+target.name()+" takes a careful step backward and throws a bottle at Maya. She catches it without looking and tosses it over her shoulder.<p>"
				+ "<i>\"Novice's work,\"</i> she says. <i>\"This isn't your first night. Act like it.\"</i><p>"
				+ ""+target.name()+" throws a punch; Maya barely seems to move and it misses her entirely. "+target.name()+" says something you don't catch, some taunt or attempt to buy time; Maya just keeps coming as if nothing was said at all. "+target.name()+" pulls out some of her toys; Maya somehow ends up holding them herself, then throws them away.<p>"
				+ "<i>\"You have to be better than this,\"</i> Maya says. <i>\"You've found teachers, haven't you? You aren't just making this up as you go?\"</i><p>"
				+ ""+target.name()+"'s running out of room. When her back hits a wall, "+target.name()+"'s nerve breaks. She throws out a fast, desperate backhand that cracks across Maya's cheek.<p>"
				+ "Maya moves with the impact, which leaves a red mark across her face, then looks back at "+target.name()+". A grin spreads across her face that shows a lot of teeth. Seeing it, you wonder just how much distance there really is between rage and arousal.<p>"
				+ "<i>\"All right,\"</i> Maya says. <i>\"Playtime's over.\"</i><p>"
				+ "Black wings erupt violently from her back, accompanied by the sound of tearing silk. Her eyes flash brilliantly, losing their pupils; her hair blows straight back from her head, carried by a wind that isn't affecting anything else.<p>"
				+ "When you think to look, "+target.name()+" stares at Maya. She's dropped her tools, her arms have gone limp, and her jaw hangs open. Whatever sense of awe and spectacle you're feeling at the sight of Maya, the expression on "+target.name()+"'s face is how you'd imagine "+target.name()+" would look in the throes of ecstatic revelation.<p>"
				+ "<i>\"Undress,\"</i> Maya says.<p>"
				+ ""+target.name()+" does at once, tearing some of her clothes in her haste to do so.<p>"
				+ "<i>\"Don't think worse of me for this,\"</i> Maya says, and slides the back of her hand against "+target.name()+"'s cheek. <i>\"This is a reasonable kindness.\"</i><p>"
				+ ""+target.name()+" is beyond speech. She rubs her skin against Maya's hand like a cat looking for attention. You can see rivulets of arousal sliding down both "+target.name()+"'s thighs.<p>"
				+ "<i>\"If you stay in these Games long enough,\"</i> Maya says, <i>\"you might learn some of what I know. In the meantime...\"</i><p>"
				+ "She pulls her hand back from "+target.name()+"'s face, makes a peculiar gesture with it, then jabs forward, poking "+target.name()+" directly below her navel.<p>"
				+ ""+target.name()+" immediately convulses and crumples into a heap. She tries to talk, but can't, and it comes out as a moan instead, then a strangled cry, and finally an outright ecstatic scream. She rolls over onto her back, settles down, then arches up off the floor as another wave of pleasure strikes her.<p>"
				+ "Maya watches this for a few seconds, then looks away... and straight at you.<p>"
				+ ""+target.name()+"'s clothes appear in her hand, neatly folded, and Maya blows you a kiss.<p>"
				+ "Then she walks away into the darkness, and is gone.";
	}

	@Override
	public String describe() {
		return "Maya is a beautiful young woman in her early twenties, though she carries herself with the grace of a more mature lady. She has soft, shapely breasts, larger than her slim frame would " +
				"imply. Her waist length, raven black hair is tied in a single braid down her back. She wears elbow-length silk gloves, giving the appearance of sensual elegance. Her eyes are a beautiful dark " +
				"blue, behind her red-framed glasses, but every so often the light catches them in a way that makes you think there might be something dangerous inside.";
	}

	@Override
	public String draw(Combat c, Result flag) {
		Character target;
		if(c.p1==character){
			target=c.p2;
		}
		else{
			target=c.p1;
		}
		if(target.human()){
			Global.flag(Flag.Clue1);
		}
		return "You've got her, you're sure of it. Maya let her guard down long enough for you to get her exactly where you want her. She falls to her hands and knees, her defenseless rear facing you. "
				+ "It's risky to attempt an insertion against someone of Maya's skill, but you can't afford to pass up any advantage you're given. Before she can regain her composure, you thrust your "
				+ "cock into her wet pussy. She lets out a yelp in surprise and pleasure, while you fuck her relentlessly. If she get free before you can make her cum, you're probably screwed, but you "
				+ "can feel that she's close to the end. Your cock warns you that you're approaching your limit and her dark aura is gradually eating away at your willpower. You need to end this right now! "
				+ "You reach down under her waist and quickly locate her love button. She moans in pleasure as you simultaneously stimulate her pussy and clit. It's working! She's already trembling on "
				+ "the verge of orgasm.<p>"
				+ "In a remarkable display of strength and desperation, she manages to wrap her legs around your hips, holding you completely inside her. You're caught completely off guard when her vaginal "
				+ "walls squeeze and milk your throbbing cock. She's cumming! If you can just hold out for a few more seconds, you'll win, but this sensation is just too much! You spurt hot jets of semen "
				+ "into Maya's spasming womb. It's a draw, snatched from the jaws of victory.<p>"
				+ "You both collapse on the floor, exhausted. You hear a lovely giggle, almost melodic, coming from Maya. Her giggles turn into unrestrained laughter as she rolls onto her back. <i>\"A draw!\"</i> "
				+ "Her mature air of dignity is gone as she shakes with mirth. <i>\"Barely a draw! I had to struggle to achieve a mutual orgasm! Against a rookie, no less! God, I haven't felt this human in "
				+ "years!.\"</i> She sits up, more breathless from her laughter than the sex.<p>"
				+ "<i>\"Sorry. It probably sounds like I'm disparaging you, but I thought there was only one man who could do that to me anymore.\"</i> She wears a delighted smile, neither dominant nor "
				+ "elegant. <i>\"I think I owe you quite a reward, but it appears your carnal desires are already sated. I know, I'll tell you a secret that Tyler would probably charge you a fortune for.\"</i><p>"
				+ "She looks at you intently, her eyes shining. <i>\"Let's start with a trivia question: in all the years these Games have been going on, how many competitors do you think have gotten "
				+ "seriously hurt?\"</i> This is a bit of an unfair question since you don't know how long the Games have been running. There can't have been too many, or it would have attracted attention. "
				+ "The matches are pretty rough and the physical combat seems to escalate over time. Probably 5-10?<p>"
				+ "<i>\"Wrong. The answer is zero. No one has ever been injured in a match. It's not just due to Lilly's hard work, as diligent as she and her predecessors have been. It's because we're "
				+ "under our Benefactor's protection.\"</i> Her expression has shifted slightly as she talks. It now appears more like hero worship, along with something else you can't quite identify. "
				+ "<i>\"There's pain, of course. If we girls couldn't take advantage of male vulnerabilities, it would interfere with match balance. Yet, no matter how hard you get hit, you'll be back "
				+ "on your feet in no time, with no long term effects.\"</i><p>"
				+ "Her smile turns wistful as she stares off into space. Her voice drops almost to a whisper. <i>\"Our Benefactor is kind. Maybe too kind. He cannot bear to see any of His chosen hurt. "
				+ "That's why He couldn't let me die.\"</i> You suddenly realize what she reminds you of. She looks and sounds like a nun at prayer. She turns her attention back to you. <i>\"I think "
				+ "that was more than one secret. I could just erase that last bit from your memory, but I'll just call it a bonus.\"</i>";
	}

	@Override
	public boolean fightFlight(Character opponent) {
		return fit();
	}

	@Override
	public boolean attack(Character opponent) {
		return true;
	}

	@Override
	public void ding() {
		int rand;
		for(int i=0; i<(Global.random(3)/2)+2;i++){
			rand=Global.random(5);
			if(rand==0){
				character.mod(Attribute.Power, 1);
			}
			else if(rand==1){
				character.mod(Attribute.Seduction, 1);
			}
			else if(rand==2){
				character.mod(Attribute.Cunning, 1);
			}
			else if(rand==3){
				character.mod(Attribute.Dark, 1);
			}
			else if(rand==4){
				character.mod(Attribute.Hypnosis, 1);
			}
		}
		character.stamina.gain(6);
		character.arousal.gain(7);
		character.mojo.gain(4);
	}

	@Override
	public String startBattle(Character opponent) {
		return "Maya smiles softly as she confidently steps toward you. <i>\"Are you simply unfortunate or were you actually hoping to challenge me? What a brave boy. I'll try not to " +
				"disappoint you.\"</i>";
	}

	@Override
	public boolean fit() {
		return !character.nude();
	}

	@Override
	public String night() {
		return null;
	}

	@Override
	public void advance(int rank) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case dominant:
			return value>=50;
		case nervous: case desperate:
			return value>=200;
		default:
			return value>=100;
		}
	}

	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case dominant:
			return 1.4f;
		case nervous: case desperate:
			return .5f;
		default:
			return 1f;
		}
	}

	@Override
	public String image() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}
	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		if(assist.human()){
			return "As you attempt to hold off Maya, you see "+target.name()+" stealthily approaching behind her. Looks like you're about to get some help. Maya suddenly " +
					"winks at you and her foot darts up almost playfully between your legs. She barely seemed to use any force, but the impact is still staggering. You " +
					"crumple to the floor, holding your balls in debilitating pain.<p>" +
					target.name()+" hesitates, realizing she doesn't have the advantage of superior numbers. Maya is already moving, though, and pounces on the other girl " +
					"before she can escape. From your position on the floor, you can't see exactly what is happening, but it's clear Maya is overwhelming her. As soon as the " +
					"pain subsides, you force yourself back to your feet. You'd hoped to team up with "+target.name()+", but she is already trembling in orgasm under Maya's " +
					"fingers. The older girl returns her attention to you and smiles. <i>\"Sorry about the interruption. Where were we?\"</i>" ;
		}
		else{
			return assist.name()+" isn't likely to be able to hold off Maya on her own. Your best bet is to work together to take her down. You creep up behind her, with Maya " +
					"showing no sign of noticing you. When you've gotten close enough, you lunge toward her, hoping to catch her from behind. To your surprise, she vanishes the " +
					"moment you touch her and you stumble forward into "+assist.name()+". You turn around and see Maya standing a couple feet away. You've lost the element of " +
					"surprise (you probably never had it), but it's still 2 on 1.<br>" +
					"You suddenly feel "+assist.name()+" grab you from behind. You turn your head and notice her eyes are dull and unfocused. Maya must have hypnotized her to " +
					"help trap you. Maya speaks up in a melodic voice. <i>\"How rude of you to interrupt a perfectly enjoyable fight. Naughty boys should be punished.\"</i> She strips off your clothes and " +
					"runs her fingers over your exposed dick. You immediately grow hard under her touch. She's too skilled with her hands for you to hold back and you're completely " +
					"unable to defend yourself. She makes you cum embarrassingly quickly and both girls discard you unceremoniously to the floor. Maya snaps her fingers in front of " +
					assist.name()+"'s face to bring her out of the trance and the girl looks down at your defeated form in confusion. <i>\"Thank you for your cooperation. Now we can " +
					"continue our fight without interruption.\"</i>";
		}
	}
	
	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.SELF_BUSTED, "<i>\"Good, this is so much more satisfying when you put up a struggle.\"</i> Maya says, trying her best to keep her composure, though you see her knees get a little weak and her voice sounds slightly choked-up.");
		return comments;
	}
	@Override
	public int getCostumeSet() {
		return 1;
	}
	@Override
	public void declareGrudge(Character opponent, Combat c) {
		// TODO Auto-generated method stub

	}
	@Override
	public void resetOutfit() {
		character.outfit[0].clear();
		character.outfit[1].clear();
        this.character.outfit[0].add(Clothing.camisole);
        this.character.outfit[0].add(Clothing.blouse);
        this.character.outfit[1].add(Clothing.lacepanties);
        this.character.outfit[1].add(Clothing.skirt);
	}
}
