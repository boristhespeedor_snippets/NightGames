package Comments;

import characters.Character;
import combat.Combat;

public class LowBlowRequirement implements CustomRequirement {

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		return c.getLowBlow(self);
	}

}
