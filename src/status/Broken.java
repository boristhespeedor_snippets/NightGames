package status;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;

public class Broken extends Status {

    public Broken(Character affected, int magnitude) {
        super("Broken", affected);
        this.magnitude = magnitude;
        this.flag(Stsflag.broken);
        duration = 999;
        decaying = false;
    }

    @Override
    public String describe() {
        return "";
    }

    public int mod(Attribute a){
        if(affected.has(Trait.enraged)) {
            if (a == Attribute.Power) {
                return magnitude / 5;
            }
            if (a == Attribute.Speed) {
                return magnitude / 5;
            }
        }else{
            if (a == Attribute.Power) {
                return magnitude / 10;
            }
            if (a == Attribute.Speed) {
                return magnitude / 10;
            }
        }
        return 0;
    }

    public int pleasure(int x, Anatomy area){

        return x * magnitude/200;
    }

    @Override
    public Status copy(Character target) {
        return new Broken(target,magnitude);
    }

}
