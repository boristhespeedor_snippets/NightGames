package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Nimble extends Status {

	public Nimble(Character affected, int duration) {
		super("Nimble",affected);
		this.duration=duration;
		flag(Stsflag.nimble);
		tooltip = "Bonus to evasion and escaping based on Animism and current Arousal";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}
		this.affected = affected;		
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You're as quick and nimble as a cat.";
		}
		else{
			return affected.name()+" darts around gracefully.";
		}
	}

	@Override
	public int evade() {
		return affected.get(Attribute.Animism)*affected.getArousal().percent()/100;
	}

	@Override
	public int escape() {
		return affected.get(Attribute.Animism)*affected.getArousal().percent()/100;
	}

	@Override
	public int counter() {
		return affected.get(Attribute.Animism)*affected.getArousal().percent()/100;
	}

	@Override
	public Status copy(Character target) {
		return new Nimble(target,duration);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,5);
		decay(c);
	}

}
