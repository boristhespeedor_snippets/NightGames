package status;

import characters.*;
import characters.Character;
import combat.Combat;

public class Buzzed extends Status {

	public Buzzed(Character affected) {
		super("Buzzed",affected);
		magnitude = 1;
		stacking = true;
		lingering = true;
		tooltip = "Penalty to Power, Cunning, and Perception. Take reduced pleasure damage";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			duration = 30;
		}else{
			duration = 20;
		};
		this.affected = affected;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You feel a pleasant buzz, which makes you a bit sluggish, but also takes the edge off your sense of touch.";
		}
		else{
			return affected.name()+" looks mildly buzzed, probably trying to dull her senses.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a == Attribute.Perception){
			return -3*magnitude;
		}
		else if(a == Attribute.Power){
			return -1*magnitude;
		}
		else if(a == Attribute.Cunning){
			return -2*magnitude;
		}
		return 0;
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		return -(x*magnitude)/10;
	}

	@Override
	public int evade() {
		return -5;
	}

	@Override
	public Status copy(Character target) {
		return new Buzzed(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,15);
		decay(c);
	}
	
}
