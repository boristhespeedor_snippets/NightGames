package status;

import characters.*;
import characters.Character;
import combat.Combat;

public class Masochistic extends Status {

	public Masochistic(Character affected) {
		super("Masochism",affected);
		flag(Stsflag.masochism);
		duration = 10;
		tooltip = "Gain Arousal whenever you take Pain damage";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			duration = 15;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Arousing fantasies of being hurt continue to tempt you.";
		}
		else{
			return affected.name()+" is still flushed with arousal at the idea of being struck.";
		}
	}

	@Override
	public int mod(Attribute a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x, Anatomy area) {
		if(area!=Anatomy.soul) {
			affected.getArousal().restore(3 * x / 2);
		}
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Masochistic(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous,15);
		decay(c);
	}

}
