package status;

import characters.Anatomy;
import characters.Character;
import characters.Trait;

public class Soulbound extends Status {
    Character target;
    public Soulbound(Character afflicted, Character target, int duration){
        super("Soulbound",afflicted);
        flag(Stsflag.soulbound);
        this.target = target;
        this.duration = duration;
        if(afflicted.has(Trait.PersonalInertia)){
            duration = 3*duration/2;
        }
    }

    @Override
    public int damage(int x, Anatomy area) {
        if(area!=Anatomy.soul) {
            target.pain(x/2,Anatomy.soul);
        }
        return 0;
    }

    @Override
    public int pleasure(int x, Anatomy area) {
        if(area!=Anatomy.soul) {
            target.pleasure(x/2,Anatomy.soul);
        }
        return 0;
    }

    @Override
    public String describe() {
        if(affected.human()) {
            return target+" is still bound to you, sharing your sensations.";
        }else{
            return "You are still feeling everything "+affected+" does.";
        }
    }

    @Override
    public Status copy(Character target) {
        return new Soulbound(affected,target,duration);
    }
}
