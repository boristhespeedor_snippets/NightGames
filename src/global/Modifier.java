package global;

public enum Modifier {
	normal	(0),		//No challenge
	pantsman (.3f),		//Only get boxers when resupplying
	nudist	(.5f),		//No clothes
	norecovery (.5f),	//Arousal only empties on loss or draw, no masturbation
	vibration (.3f), 	//Small arousal gain in upkeep
	vulnerable (.25f),	//Permanent Hypersensitive effect
	pacifist (.25f),	//Unable to use pain attacks
	notoys (.15f),		//Unable to use toys
	noitems (.15f),		//Unable to use consumables
	mittens (.3f),		//Hand proficiency 25%
	lameglasses (.5f), 	//Negates Mojo gain
	hairtie (.2f),		//Increased pain damage to groin
	ticklish (.2f),		//Increased temptation and weakness damage from Tickle
	furry (.3f),		//All opponents in BeastForm
	slippery (.2f),		//Permanently oiled
	ftc (0),			//Fuck the Carrier
	maya(0),			//Maya Exhibition match
	quiet(0),			//AI only match
	;
	private float bonus;
	private Modifier(float bonus){
		this.bonus=bonus;
	}
	public float bonus(){
		return this.bonus;
	}
	public String percentage(){
		return String.format("%d", Math.round(bonus*100));
	}
}
