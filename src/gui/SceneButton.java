package gui;

import global.Global;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class SceneButton extends KeyableButton {
	private String choice;
	public SceneButton(String label) {
		super(label);
		setFont(new Font("Georgia", 0, 18));
		this.choice = label;
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Global.current.respond(SceneButton.this.choice);
			}
		});
	}
	public SceneButton(String label, String description) {
		this(label);
		this.setToolTipText(description);
	}
}
