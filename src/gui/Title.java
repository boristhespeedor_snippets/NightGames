package gui;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

import global.Global;
import global.SaveManager;

import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.JLabel;
import java.awt.Font;

public class Title extends JPanel {
	private int width;
	private int height;
	private GUI gui;
	final Image titleImage;
	
	public Title(GUI gui, int width, int height) {
		this.gui = gui;
		this.width = width;
		this.height = height;
		setBackground(new Color(0,6,18));

		titleImage = getTitleImage();
	
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		Component verticalStrut = Box.createVerticalStrut(400);
		add(verticalStrut);
		
		Box horizontalBox = Box.createHorizontalBox();
		add(horizontalBox);
		
		JButton btnNewGame = new JButton("New Game");
		btnNewGame.setFont(new Font("Georgia", Font.BOLD, 18));
		horizontalBox.add(btnNewGame);
		btnNewGame.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		Component horizontalStrut = Box.createHorizontalStrut(60);
		horizontalBox.add(horizontalStrut);
		
		JButton btnLoad = new JButton("Load");
		btnLoad.setFont(new Font("Georgia", Font.BOLD, 18));
		horizontalBox.add(btnLoad);
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Global(Title.this.gui);
				SaveManager.load();
				Title.this.setVisible(false);
			}
		});
		btnLoad.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(60);
		horizontalBox.add(horizontalStrut_1);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setFont(new Font("Georgia", Font.BOLD, 18));
		horizontalBox.add(btnExit);
		btnExit.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnNewGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Global(Title.this.gui);
				Title.this.setVisible(false);
			}
		});
	}

	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(titleImage, 0, 0, null);
    }
	
	private Image getTitleImage() {
		Image image = null;

        try {
        	URL titleurl = getClass().getResource(
					"assets/Title.png");
        	if(titleurl!=null){
        		image = ImageIO.read(titleurl);
        	}
            image = image.getScaledInstance(width, height-30, image.SCALE_FAST);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
	}
}
