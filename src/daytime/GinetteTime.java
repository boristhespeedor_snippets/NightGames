package daytime;

import characters.Character;
import characters.ID;
import global.Flag;
import global.Global;
import global.Roster;

public class GinetteTime extends Activity{
    boolean acted;
    public GinetteTime(Character player) {
        super("Ginette", player);
    }

    @Override
    public boolean known() {
        return Global.checkFlag(Flag.metGinette);
    }

    @Override
    public void visit(String choice) {
        Global.gui().clearText();
        Global.gui().clearCommand();
        if(choice=="Start"){
            acted = false;
        }
        if(choice=="Leave"){
            done(acted);
            return;
        }

        if(Roster.getAffection(ID.PLAYER,ID.GINETTE)<1){
            Global.gui().message("You meet up with Ginette at the Student Union. She's got a small storage room basically to herself, where she organizes the item cache boxes. It's a bit cramped, but very private. She seems a bit nervous about your company in such a cramped space.<br>" +
                    "<br>" +
                    "The shelves around you are packed with boxes filled with familiar potions, sexy clothes, and random junk. Who picks out this stuff?<br>" +
                    "<br>" +
                    "<i>\"I do.\"</i> She fiddles with her ponytail and looks away, embarrassed. <i>\"I asked some of the older competitors what stuff they used to buy for the Games. I also look around obscure stores and websites for anything that might be useful to you guys.\"</i><br>" +
                    "<br>" +
                    "That's a lot of responsibility for a freshman. They must have a lot of faith in her.<br>" +
                    "<br>" +
                    "<i>\"You know my big sister is the supervisor, right? Besides, this isn't really an established job. When Lilly recruited me, I just ran around cleaning up after the match. I thought scattering some free items in the match area would help spice things up a bit. I mentioned it to Lilly, and she got me a small budget to buy stuff.\"</i><br>" +
                    "<br>" +
                    "Well, it was a good idea she had. Was putting them in puzzle boxes her idea too? <i>\"No, that was Lilly's idea. She thought giving you guys stuff for free was too easy. She wanted you to earn it in some way.\"</i> <br>" +
                    "<br>" +
                    "That sounds like what you know about Lilly. Well, you can't complain too much. It is still some extra help during matches.<br>" +
                    "<br>" +
                    "You look through the nearby shelves. Some of this stuff is pretty easy to buy in nearby stores, but some of it you've never seen for sale. Frankly, you'd like to see more obscure equipment in the caches.<br>" +
                    "<br>" +
                    "<i>\"Yeah, I figured. Unfortunately the rare stuff is pretty pricey. If I want to keep planting as many caches as I currently do, some of them need to be filled with the cheap stuff. Like I said: small budget.\"</i><br>" +
                    "<br>" +
                    "How small?<br>" +
                    "<br>" +
                    "<i>\"Well, better than the average extracurricular activity budget, but still pretty small compared to what you Fuck Buddies earn.\"</i> She seems to be comfortable calling you that to your face now. <i>\"I don't suppose you'd consider donating some of your winnings to expand my budget?\"</i> She gives you a pretty persuasive puppy-dog stare.<br>" +
                    "<br>" +
                    "Well, it's not like you're just banking your prize money. If you want to stay competitive, there's a lot of important things you need to spend that money on.<br>" +
                    "<br>" +
                    "<i>\"Improving my budget could be mutually beneficial. I'm pretty sure I have sources that you wouldn't be able to buy from directly. Just consider it an investment in getting more rare loot.\"</i><br>");
            Roster.gainAffection(ID.PLAYER,ID.GINETTE,1);
            Global.gui().message("<b>You've gained Affection with Ginette</b>");
        }else if(choice.startsWith("More Caches")){
            if(player.money >= 1500){
                player.money -= 1500;
                Global.gui().message("You give Ginette a wad of money so she can put together more item caches each match. <br>" +
                        "<br>" +
                        "<i>\"This is... Wow. This will help my budget a lot. Yeah, I can definitely afford to deploy more caches at night.\"</i> She smiles at you, shyly. <i>\"The guys who help me place them may not appreciate the extra work, but I do. It feels like my item cache idea could actually become more of an important part of the Games.\"</i><br>" +
                        "<br>" +
                        "She puts the money in a small lockbox and fidgets awkwardly for a moment.<br>" +
                        "<br>" +
                        "<i>\"Can I... Can I kiss you? On the cheek, I mean!\"</i> She flushes bright red. <i>\"I should have just done it. It probably wouldn't be this awkward, right?\"</i><br>" +
                        "<br>" +
                        "While she's flustered and babbling, you turn your cheek toward her. She hesitates for a moment, but gives you a light peck.<br>" +
                        "<br>" +
                        "<i>\"Thanks.\"</i> She grins at you, still beet red. <i>\"I really appreciate it.\"</i><br>");
                Roster.gainAffection(ID.PLAYER,ID.GINETTE,3);
                Global.gui().message("<b>You've gained Affection with Ginette</b>");
                Global.flag(Flag.MoreCache1);
            }else{
                Global.gui().message("You don't have enough money");
            }
        }else if(choice.startsWith("No Basic Items")) {
            if (player.money >= 2500) {
                player.money -= 2500;
                Global.gui().message("You hand Ginette a modest bundle of money, and her eyes widen when she counts it. Hopefully with this she won't have to stock so many common items for a while?<p>" +
                        "<i>\"Whoa! Are you serious?! You're just going to give me this?\"</i> She stares at you with a combination of surprise and gratitude. It shouldn't be such a shock. As she said, investing in her budget is mutually beneficial.<p>" +
                        "<i>\"I know I said that, but... I mean, yes there will be better stuff available, but your opponents are probably going to get their share too. You could just spend it on yourself...\"</i> She hesitates until it's clear you aren't going to ask for the money back, then smiles genuinely.<p>" +
                        "<i>\"I guess I shouldn't try to talk you out of it. Thanks. No more cheap flasks and potions in the care packages. I'll sell my current stock and make this money last as long as it needs to.\"</i>");
                Roster.gainAffection(ID.PLAYER, ID.GINETTE, 3);
                Global.gui().message("<b>You've gained Affection with Ginette</b>");
                Global.flag(Flag.CacheNoBasics);
            } else {
                Global.gui().message("You don't have enough money");
            }
        }else if(Roster.getAffection(ID.PLAYER,ID.GINETTE)>=10 && !Global.checkFlag(Flag.CacheFinder)){
            Global.gui().message("When you meet Ginette at the usual location. She seems even more fidgety than usual. She obviously has something to say, but you don't press her on it until she brings it up.<p>" +
                    "<i>\"So... Lilly is very insistent that the Games be fair. Matches are supposed to be pure tests of strategy and skill. But then, she gives you those handicaps, so 'fair' doesn't always mean treating every Fuck Buddy the same.\"</i><p>" +
                    "<i>\"Besides, these item caches aren't even an official part of the Games. They're just something I came up with to spice things up. You've helped a lot to improve these, so I think it's 'fair' if I send you a text to let you know when we place one.\"</i><p>" +
                    "Well, if she wants to show some favoritism, you aren't going to argue. You keep your phone on you during matches anyway, so you'll just keep an eye open for her texts.<p>" +
                    "<i>\"Great! Um... Maybe don't mention this to Lilly or the other competitors, just in case they have a different idea of 'fair'.");
            Global.flag(Flag.CacheFinder);
        }else{
            Global.gui().message("You meet Ginette at the Student Union and help her pack some item caches. She still acts pretty shy, but her body language makes it clear that she enjoys the company. <br>" +
                    "<br>" +
                    "At one point, she loses her balance grabbing something off a high shelf. You catch her before she gets hurt, and she thanks you while blushing furiously.<br>" +
                    "<br>" +
                    "She's really cute, and it's kind of refreshing having an opportunity to flirt so innocently. Most of your romantic interests are based on competitive sex. Of course, Ginette here watches your sexfights every night, so maybe it's not quite so innocent from her perspective.<br>");
            if(!Global.checkFlag(Flag.MoreCache1)){
                Global.gui().choose(this,"More Caches: $1500");
            }
            if(!Global.checkFlag(Flag.CacheNoBasics)){
                Global.gui().choose(this,"No Basic Items: $2500");
            }
            if(Roster.getAffection(ID.PLAYER,ID.GINETTE)>=12){
                Global.gui().message("You come by the Student Union again to talk to Ginette. When you enter the storage room you're treated to the sight of a small tight butt wiggling over a rather huge cardboard box. While you appreciate the view, your surprise entry causes Ginette to flail wildly around the box. <p>" +
                        "She lands squarely on her butt, letting out a cute squeak as she does. She looks left and right to locate who came in and blushes as she realizes you saw everything. <p>" +
                        "<i>\"Hey,\"</i> She waves her hand nervously. Your eyes track her hand and notice the small pink egg vibrator trapped between her fingers. When she follows your gaze, she spots the same toy in her hands and looks as if she wants to melt into the ground. <p>" +
                        "<i>\"I-it's not what it looks like!\"</i> You can't help but laugh. She rushes up to you and begins shaking you but it doesn't stop you, <i>\"S-stop it! Stop laughing!\"</i> <p>" +
                        "Oh , she's just too cute. You place your hands over hers and gently pull the egg out of her hand. You ask what this is about. <p>" +
                        "<i>\"It's part of the new caches, stupid,\"</i> She murmurs, crossing her arms. Huh, that's surprising. Usually you have to buy your own equipment. They've never just given out toys before. <p>" +
                        "<i>\"The money you gave earlier helped me get some connections for some sex toys,\"</i> She answers while putting on a more professional air, <i>\"These ones are kind of weak but they can stick harmlessly onto your body. Put them on some erogenous zones and you can focus on different areas of your opponents.\"</i> <p>" +
                        "You look up to Ginette to find her trying her best to look stoic but her skin is still red. You take pity and play along. You turn on the vibration of the egg to find it actually kind of weak. <p>" +
                        "<i>\"What? I paid good money on those contracts. Give me that,\"</i> She snatches the egg out of your hand, staring hard at it. <p>" +
                        "<i>\"Maybe it works better when used on a person,\"</i> She whispers. That's an idea. You ask for the egg back intending to test it on someone in your free time. She stares at you like you just slapped her. <p>" +
                        "<i>\"What! What do you mean ‘testing it'?!\"</i> She uses her arms to cover her breasts. You quickly clarify what you meant. <p>" +
                        "<i>\"Oh, you mean with one of your Fuck Buddies. Right,\"</i> She bites her lip as she slowly removes her hands. There's a moment of awkward silence as neither of you are quite sure how to continue this conversation. It's her suddenly paling that gets your attention. <p>" +
                        "<i>\"Oh no. Lilly wanted a list of the stuff for tonight in an hour. If these things are a dud she might not trust me with more money.\"</i> <p>" +
                        "Oh, well then. You pull out your phone and go down your contact list. Surely someone is nearby to act as a test subject. <p>" +
                        "<i>\"No time,\"</i> Ginette grabs your hand, turns on the vibrator, and firmly plants it on her breast, <i>\"Use this thing and make me cum.\"</i> <p>" +
                        "You blink at the sudden request. You ask if she's sure. <p>" +
                        "<i>\"Hell no,\"</i> She says as her whole body shakes nervously, <i>\"But I don't want to disappoint my sis. If you can make me cum in five minutes then I know it won't be a bust.\"</i> <p>" +
                        "She's begging you with her puppy dog eyes. You remind her that you're a combatant in the games and that you will not be holding back. <p>" +
                        "<i>\"Hey, I watch the games all the time,\"</i> She says with false bravado, <i>\"I think I can handle a little petting.\"</i> <p>" +
                        "With her consent you walk up behind her with the vibrator and immediately go to work. You push the weak vibrator on to one of her boobs while kneading both with your hands. Ginette's voice immediately rises a few octaves as she's unprepared for your techniques. With the vibrations moving in and out with your touch, she's left whimpering and squirming as you do everything you can to make her cum. <p>" +
                        "You slide the hand without the vibrator down, aiming for her pants. You find a weak hand stopping the descent. Ginette's eyes are closed but she shakes her head. <p>" +
                        "<i>\"Not there. Not right now. Boobs only.\"</i> You relent and go back to messaging her chest. You're glad you get to do this with her, but your boner is getting obvious and is involuntarily poking her ass. She gasps but doesn't say anything as you rub softly against her. <p>" +
                        "You feel one of her hands on your crotch and the sure sounds of your zipper falling. Her hand softly caresses your dick, awkwardly fumbling around your tip. It's awkward, her nails occasionally skim the surface, and she flinches every now and then from your cock twitches, but she's trying her best. Still, comparing experiences, you outmatch her and she soon cums way sooner then the five minute limit. <p>" +
                        "Ginette loses feelings in her legs, causing her to fall backwards on to you. You do your best to let her down gently, which is rather difficult as she still has a light grip on your dick. When she's sitting down on the ground, her eyes are glued onto the fact that she has your dick hostage. She begins stroking it with one hand, mesmerized by your rod. <p>" +
                        "Seeing as you have no reason to hold back, you come quickly. Streams of cum escape and hit the floor of the storage room, with some landing straight into Ginette's hands. She stares at it in shock. <p>" +
                        "<i>\"So this is what it feels like,\"</i> She murmurs as her tongue begins to lap it up. Her eyes widen and she begins coughing. <p>" +
                        "<i>\"It's bitter!\"</i> She looks as you, betrayed. You honestly don't know how to react. You just respond by saying that you think the vibrator works. <p>" +
                        "<i>\"Oh, right!\"</i> She tries to stand up, only to trip forward. You catch her before she falls, causing her to blush as she looks up to you, <i>\"I've never cum that hard before. Um, so, I think it works. Or it might have been you. Argh, I don't know. I'll think about it some more\"</i> <p>" +
                        "You smile at her panicking. It really is adorable. You zip up your pants and point towards your cum on the floor. You ask if you should help with that. <p>" +
                        "<i>\"I'm part of the nightly clean up crew. This is easy,\"</i> She nervously shuffles you towards the door, <i>\"Anyways thanks for helping today. Hope the rest of the day treats you well.\"</i> <p>" +
                        "And with that she closes the door. You turn around, concerned you did something wrong, until she opens the door and gives you a small smile and nod. Then she slams the door. <p>");
                Roster.gainAffection(ID.PLAYER, ID.GINETTE, 2);
                Global.gui().message("<b>You've gained Affection with Ginette</b>");
            }
            Roster.gainAffection(ID.PLAYER,ID.GINETTE,1);
        }
        Global.gui().choose(this,"Leave");
    }

    @Override
    public void shop(Character npc, int budget) {

    }
}
